##MediaMonks  
### FrontEnd-Test 
##### Por Gastón de la Llana

------

#### Install
```
npm install
```


#### Run dev server
Starts a server at `http://localhost:8080/`

```
npm run serve
```

#### Run build
Compile all the files and generates a folder for production

```
npm run build
```

#### Run server build
Starts a server on build folder at `http://localhost:9000/`
```
npm run serve-build
```