'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const cssnano = require('gulp-cssnano');
const uglifyes = require('uglify-es');
const composer = require('gulp-uglify/composer');
const uglify = composer(uglifyes, console);
const gulpIf = require('gulp-if');
const del = require('del');
const refHash = require('gulp-ref-hash');
const useref = require('gulp-useref');
const browserify = require('gulp-browserify');
const imagemin = require('gulp-imagemin');
const cache = require('gulp-cache');
const babel = require('gulp-babel');
const vueify = require('gulp-vueify2');


// ----------------------------------------//
// ------------ Development TASKS ---------------//
// ----------------------------------------//

// start server with browsersync
gulp.task('server', function () {
  browserSync.init({
    server: {
      baseDir: 'app',
    },
    watch: true,
    open: false,
    port: 8080
  })

  gulp.watch([ "app/css/**/*.css"], function(){
    browserSync.reload();
  });

  gulp.watch(["app/scss/**/*.scss"], gulp.series('sass'));


});

// preprocessor for sass to css
gulp.task('sass', function () {
  return gulp.src('app/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    // .pipe(browserSync.reload({
    //   stream: true
    // }))
});

// preprocessor to convert ECMAScript 2015+ code into a backwards compatible 
gulp.task('babel', () =>
  gulp.src('app/js/**/*.js')
    .pipe(babel({
      "presets": ["env"]
    }))
    .pipe(
      browserify({
        insertGlobals: true
      })
    )
    .pipe(gulp.dest('app/dist'))
    .pipe(browserSync.reload({
      stream: true
    }))
);


// ----------------------------------------//
// ------------ BUILD TASKS ---------------//
// ----------------------------------------//

// task for the build copy all the stuff in the image/assets folder
gulp.task('images', function () {
  return gulp.src('app/assets/**/*.+(png|jpg|jpeg|gif|svg|ico|woff)')
    .pipe(cache(imagemin({
      interlaced: true
    })))
    .pipe(gulp.dest('build/assets'))
});



// copy all the css/js files and minimize it
gulp.task('copiar', function () {
  return gulp.src('app/*.html')
    .pipe(refHash({
      paths: {
        js: './dist/',
        css: './css/',
      }
    }))
    .pipe(useref())
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest('build'))
});

// delete the build folder if exist
gulp.task('clean:build', function () { // limpia archivos no usados del build
  return del('build');
})

// start server watching the 'build' folder with browsersync
gulp.task('server-build', function () {
  browserSync.init({
    server: {
      baseDir: 'build',
    },
    // proxy: '127.0.0.1:8020',
    open: false,
    port: 9000
  })
});

// ----------------------------------------//
// ------------ BUILD TASKS ---------------//
// ----------------------------------------//

// primary task to run de web server
gulp.task('start', gulp.series(gulp.parallel('server', 'sass', 'babel')), function () {
 
  gulp.watch('app/scss/**/*.scss', gulp.series('sass'));
  gulp.watch('app/js/**/*.js', gulp.series('babel'));
  gulp.watch('app/*.html', browserSync.reload);
});

// create the build folder with all the files minimized
gulp.task('build', gulp.series('clean:build', gulp.parallel('sass', 'copiar', 'images')), function () { })