const steps = [
	{ text: "We are breaking our vow of silence", position:'' , imgPercent: 0 },
	{ text: "Talent is given true skill is earned",  position:'left' , imgPercent: 11 },
	{ text: "Be flexible to change sturdy in conviction", position:'left', imgPercent: 24 },
	{ text: "Use many skills yet work as one", position:'right', imgPercent: 35 },
	{ text: "To master anything find interest in everything", position:'right', imgPercent: 50 },
	{ text: "Individuals flourish if culture and wisdom are shared", position:'right', imgPercent: 60 },
	{ text: "Train for perfection but aim for more", position:'left', imgPercent: 80 },
	{ text: "Take pride in your work but do not seek praise", position:'left', imgPercent: 100 },
	{ text: "Temporary sacrifice brings lasting results", position:'left', imgPercent: 100 },
	{ text: "", position:'', imgPercent: 110 }
];

// variable for store actual step
let actual = 0;


const app = {

	//init the app
	init() {
		this.startPreloader();

		// add the listeners for te previus boton and slides the background image
		document.getElementById('goPrev').addEventListener("click", () => {
			this.changeStep('prev');
		});

		// add the listeners for te next boton and slides the background image
		document.getElementById('goNext').addEventListener("click", () => {
			this.changeStep('next');
		});

		// click and go to an specific step for the list steps numbers at the bottom of the page
		document.querySelectorAll('.step').forEach(element => {
			element.addEventListener("click", () => {
				document.getElementById('stp'+actual).classList = 'step';
				actual = +element.innerHTML - 1;
				this.changeStep('next');
			});
		});
	},

	
	// initalize and simulate a download %
	startPreloader() {
		let counter = 0;
		let preload = document.getElementById('preload-monk');
		let preloadTxt = document.querySelector('.text-preload');
		let wrapper = document.getElementById('wrapper');
		let percentLoaded = document.getElementById('percentLoaded');

		// simulate the time of the download in miliseconds
		setInterval(function () {
			counter++;
			checkStatus();
		}, 100);

		// change de status of the text and hide the preload at 100%
		function checkStatus() {
			if (counter === 50) {
				preloadTxt.innerHTML = 'Patience, young padawan...'
			} else if (counter === 100) {
				preload.classList = 'hide';
				wrapper.classList = 'show';
			} else if (counter < 100) {
				percentLoaded.innerHTML = counter;
			}
		}
	},


	// change the background images and change elements of the interface
	changeStep(direction) {
		let titles = document.getElementById('titles');
		let backgroundImg = document.getElementById('background');
		let legend = document.getElementById('legend');
		let stepsLegend = document.getElementById('stepsLegend');
		let goPrev = document.getElementById('goPrev');
		let goNext = document.getElementById('goNext');
		let contact = document.getElementById('contact');


		document.getElementById('stp'+actual).classList = 'step'; 
		titles.style.opacity = 0;
		setTimeout(function(){ showTitle() }, 500); //delay to hide/show titles h1

		//validate if is "next" or "previus" event o button
		if (direction === 'next' && actual < steps.length) {
			actual++;
		} else if (actual > 0) {
			actual--;
		}

		document.getElementById('stp'+actual).classList = 'step active';
		backgroundImg.style.backgroundPosition =  steps[actual].imgPercent + "% 0px";
		stepsLegend.innerHTML = 'Step out '+actual+' of '+ (steps.length -2) +' on the path to digital enlightnment.';

		// conditional for hide/show elements of the interface
		if(actual === 0){
			legend.classList = 'show';
			stepsLegend.classList = 'hide';
			goPrev.classList = 'arrow prev hide';
			goNext.classList = 'arrow next show';
		}else if(actual === 9){
			goNext.classList = 'arrow next hide';
			contact.classList = 'show';
		}else{
			legend.classList = 'hide';
			stepsLegend.classList = 'show';
			goPrev.classList = 'arrow prev show';
			goNext.classList = 'arrow next show';
			contact.classList = '';
		}

		// show the title when the animation stops
		function showTitle(){
			titles.classList = steps[actual].position;
			titles.innerHTML = steps[actual].text;
			titles.style.opacity = 1;		
		}

	}
}

app.init();